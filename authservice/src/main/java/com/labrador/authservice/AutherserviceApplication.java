package com.labrador.authservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutherserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutherserviceApplication.class, args);
	}
}
