import axios from 'axios'
import qs from 'qs'
import cookie from 'js-cookie'
import jwtDecoder from 'jwt-decode'

function extractTokens (response) {
  return {
    accessToken: response.data.access_token,
    refreshToken: response.data.refresh_token,
    exp: new Date().getTime() + (1000 * (response.data.expires_in - 3))
  }
}
function saveTokens (tokens) {
  cookie.set('access_token', tokens.accessToken, {expires: new Date(tokens.exp)})
  cookie.set('refresh_token', tokens.refreshToken, {expires:  new Date(tokens.exp)})
}

export async function getAccessTokenPassword (username, password, clientId, clientSecret) {
  const data = {
    grant_type: 'password',
    username: username,
    password: password
  }
  let tokens = null
  await axios.post('/auth-api/oauth/token', qs.stringify(data), {
    auth: {
      username: clientId,
      password: clientSecret
    }
  }).then((resp) => {
    tokens = extractTokens(resp)
    saveTokens(tokens)
  }).catch((error) => {
    if (error.response.status === 400) {
      throw new Error('用户名和密码不匹配。')
    } else {
      throw new Error('发生了未知错误。')
    }
  })
  return tokens
}

export async function getAccessTokenCode (code, clientId, clientSecret) {
  const data = {
    grant_type: 'authorization_code',
    redirect_uri: 'http://localhost:8080/code_callback',
    code
  }
  let tokens = null
  await axios.post('/auth-api/oauth/token', qs.stringify(data), {
    auth: {
      username: clientId,
      password: clientSecret
    }
  }).then((resp) => {
    tokens = extractTokens(resp)
    saveTokens(tokens)
    console.log(tokens)
  }).catch((error) => {
    if (error.response.status === 400) {
      throw new Error('用户名和密码不匹配。')
    } else {
      throw new Error('发生了未知错误。')
    }
  })
  return tokens
}
export async function refreshToken(refreshToken, clientId, clientSecret){
  const data = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken
  }
  let tokens = null
  await axios.post('/auth-api/oauth/token', qs.stringify(data), {
    auth: {
      username: clientId,
      password: clientSecret
    }
  }).then((resp) => {
    tokens = extractTokens(resp)
    saveTokens(tokens)
  }).catch((error) => {
    if (error.response.status === 400) {
      throw new Error('用户名和密码不匹配。')
    } else {
      throw new Error('发生了未知错误。')
    }
  })
  return tokens
  
}
export function decodeJWTToken (jwtToken) {
  return jwtDecoder(jwtToken)
}