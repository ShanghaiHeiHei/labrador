import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import PasswordLogin from '@/components/password/PasswordLogin'
import CodeCallback from '@/components/authorizationCode/CodeCallback'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/home',
      name: 'home',
      component: Home
    },
    {
      path: '/passwordLogin',
      name: 'passwordLogin',
      component: PasswordLogin
    },
    {
      path: '/code_callback',
      name: 'codeCallback',
      component: CodeCallback,
      props: (route) => ({code: route.query.code, state: route.query.state})
    },
    {
      path: '/*',
      redirect: {name: 'home'}
    }
  ]
})
