package com.labrador.accountservice.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {
    @GetMapping("accessAuthenticated")
    public String accessAuthenticated(){
        return "accessAuthenticated";
    }

    @GetMapping("accessPermitAll")
    public String accessPermitAll() {
        return "accessPermitAll";
    }
    @GetMapping("accessOnlyAdminRole")
    public String accessOnlyAdminRole(){

        return "accessOnlyAdminRole";
    }

    @GetMapping("accessOnlyUserRole")
    public String accessOnlyUserRole() {
        return "accessOnlyUserRole";
    }

    @GetMapping("accessOnlyUserRoleAndOrgUser")
    public String accessOnlyUserRoleAndOrgUser() {
        return "accessOnlyUserRoleAndOrgUser";
    }

    @GetMapping("accessOnlyClientWithAdminRoleAndUserRole")
    public String accessOnlyClientWithAdminRoleAndUserRole() {
        return "accessOnlyClientWithAdminRoleAndUserRole";
    }

    @GetMapping("accessClientMustHasRoleAdmin")
    public String accessClientMustHasRoleAdmin(){
        return "accessClientMustHasRoleAdmin";
    }

    @GetMapping("accessClientMustHasReadScope")
    public String accessClientMustHasReadScope(){
        return "accessClientMustHasReadScope";
    }
}
