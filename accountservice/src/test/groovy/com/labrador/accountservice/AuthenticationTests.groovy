//package com.labrador.accountservice
//
//import com.jayway.jsonpath.JsonPath
//import com.jayway.jsonpath.internal.JsonContext
//import groovy.json.JsonSlurper
//import groovyx.net.http.ChainedHttpConfig
//import groovyx.net.http.FromServer
//import groovyx.net.http.HttpBuilder
//import groovyx.net.http.NativeHandlers
//import org.apache.commons.codec.Charsets
//import org.junit.jupiter.api.BeforeAll
//import org.junit.jupiter.api.Test
//import org.junit.jupiter.api.TestInstance
//import org.junit.jupiter.api.extension.ExtendWith
//import org.springframework.beans.factory.annotation.Autowired
//import org.springframework.boot.test.context.SpringBootTest
//import org.springframework.security.web.FilterChainProxy
//import org.springframework.test.context.junit.jupiter.SpringExtension
//import org.springframework.test.web.servlet.MockMvc
//import org.springframework.test.web.servlet.ResultActions
//import org.springframework.test.web.servlet.setup.MockMvcBuilders
//import org.springframework.web.context.WebApplicationContext
//
//
//import static org.assertj.core.api.AssertionsForClassTypes.assertThat
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
//
//@SpringBootTest(classes = AccountServiceApplication.class)
//@ExtendWith(SpringExtension.class)
//@TestInstance(TestInstance.Lifecycle.PER_CLASS)
//class AuthenticationTests {
//    private static final String ADMIN_USERNAME = 'admin'
//    private static final String ADMIN_PASSWORD = 'password'
//    private static final String USER_USERNAME = 'user'
//    private static final String USER_PASSWORD = 'password'
//    private static final String ORG_USERNAME = 'org:user'
//    private static final String ORG_PASSWORD = 'password'
//    private static final String ACCESS_PERMIT_ALL_URL = '/test/accessPermitAll'
//    private static final String ACCESS_PERMIT_ALL_MESSAGE = 'accessPermitAll'
//    private static final String ACCESS_ONLY_ADMIN_ROLE_URL = '/test/accessOnlyAdminRole'
//    private static final String ACCESS_ONLY_ADMIN_ROLE_MESSAGE = 'accessOnlyAdminRole'
//    private static final String ACCESS_ONLY_USER_ROLE_URL = '/test/accessOnlyUserRole'
//    private static final String ACCESS_ONLY_USER_ROLE_MESSAGE = 'accessOnlyUserRole'
//    private static final String ACCESS_AUTHENTICATED_URL = '/test/accessAuthenticated'
//    private static final String ACCESS_AUTHENTICATED_MESSAGE = 'accessAuthenticated'
//    private static final String ACCESS_ONLY_USER_ROLE_AND_ORG_USER_URL = '/test/accessOnlyUserRoleAndOrgUser'
//    private static final String ACCESS_ONLY_USER_ROLE_AND_ORG_USER_MESSAGE = 'accessOnlyUserRoleAndOrgUser'
//    private static final String ACCESS_CLIENT_MUST_HAS_ADMIN_ROLE_URL = '/test/accessClientMustHasAdminRole'
//    private static final String ACCESS_CLIENT_MUST_HAS_ADMIN_MESSAGE = 'accessClientMustHasAdminRole'
//    private static final String ACCESS_CLIENT_MUST_HAS_READ_SCOPE_URL = '/test/accessClientMustHasReadScope'
//    private static final String ACCESS_CLIENT_MUST_HAS_READ_SCOPE_MESSAGE = 'accessClientMustHasReadScope'
//
//
//
//    private MockMvc mockMvc
//
//    @Autowired
//    private WebApplicationContext wac;
//
//    @Autowired
//    private FilterChainProxy springSecurityFilterChain;
//
//
//    def getToken(String username, String password){
//        getTokenWithClient("accountservice", "password", username, password)
//    }
//
//    def getTokenWithClient(String clientId, String clientSecret, String username, String password) {
//        HttpBuilder.configure {
//            request.accept = 'application/json'
//            request.charset = Charsets.UTF_8
//            request.contentType = 'application/x-www-form-urlencoded'
//            request.encoder 'application/x-www-form-urlencoded', NativeHandlers.Encoders.&form
//        }.post() {
//            request.auth.basic clientId, clientSecret
//            request.body = [grant_type: 'password', username: username, password: password]
//            request.uri = 'http://localhost:6060'
//            request.uri.path = '/oauth/token'
//            response.parser('application/json'){ ChainedHttpConfig cfg, FromServer fs ->
//                new JsonSlurper().parseText(fs.inputStream.text)
//            }
//            response.failure { fs, body ->
//                throw new RuntimeException('登录失败，用户名或密码错误')
//            }
//            response.exception { e ->
//                throw e
//            }
//
//        }
//    }
//
//    def configMock(){
//        this.mockMvc = MockMvcBuilders.webAppContextSetup(wac)
//                .addFilter(springSecurityFilterChain).build()
//    }
//
//    @BeforeAll
//    void setup(){
//        configMock()
//    }
//
//    @Test
//    void testAccessWithQueryParamSuccess() {
//        String accessToken = getToken(ADMIN_USERNAME, ADMIN_PASSWORD).access_token
//        assertThat(accessToken).isNotBlank()
//
//        // 通过query param传递access_token
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_ONLY_ADMIN_ROLE_URL)
//                        .param("access_token", accessToken)
//        ).andExpect(status().isOk())
//
//        assertThat(result.andReturn().getResponse().getContentAsString()).isEqualToIgnoringCase(ACCESS_ONLY_ADMIN_ROLE_MESSAGE)
//    }
//
//    @Test
//    void testAccessWithHeaderSuccess() {
//        String accessToken = getToken(ADMIN_USERNAME, ADMIN_PASSWORD).access_token
//        // 通过Header传递AccessToken
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_ONLY_ADMIN_ROLE_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isOk())
//        assertThat(result.andReturn().getResponse().getContentAsString()).isEqualToIgnoringCase(ACCESS_ONLY_ADMIN_ROLE_MESSAGE)
//    }
//
//    @Test
//    void testAccessOnlyAdminRoleWithoutPermission() {
//        String accessToken = getToken(USER_USERNAME, USER_PASSWORD).access_token
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_ONLY_ADMIN_ROLE_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isForbidden())
//        .andExpect(status().is(403))
//        JsonContext context = JsonPath.parse(result.andReturn().getResponse().getContentAsString())
//        assertThat(context.<String>read('error')).isEqualToIgnoringCase('access_denied')
//        assertThat(context.<String>read('error_description')).isEqualToIgnoringCase('Access is denied')
//    }
//
//    @Test
//    void testAccessOnlyUserRoleAndOrgUser(){
//        String accessToken = getToken(ORG_USERNAME, ORG_PASSWORD).access_token
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_ONLY_USER_ROLE_AND_ORG_USER_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isOk())
//        assertThat(result.andReturn().getResponse().getContentAsString()).isEqualTo(ACCESS_ONLY_USER_ROLE_AND_ORG_USER_MESSAGE)
//    }
//
//    @Test
//    void testAccessOnlyUserRoleAndOrgUserWithoutPermission(){
//        String accessToken = getToken(USER_USERNAME, USER_PASSWORD).access_token
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_ONLY_USER_ROLE_AND_ORG_USER_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isForbidden())
//        .andExpect(status().is(403))
//    }
//
//    @Test
//    void testAccessAuthenticatedSuccess(){
//        String accessToken = getToken(USER_USERNAME, USER_PASSWORD).access_token
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_AUTHENTICATED_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isOk())
//        String respString = result.andReturn().getResponse().getContentAsString()
//        assertThat(respString).isEqualTo(ACCESS_AUTHENTICATED_MESSAGE)
//    }
//
//    @Test
//    void testAcessPermitAllSuccess(){
//        ResultActions result = mockMvc.perform(
//                get(ACCESS_PERMIT_ALL_URL)
//        ).andExpect(status().isOk())
//        String respString = result.andReturn().getResponse().getContentAsString()
//        assertThat(respString).isEqualTo(ACCESS_PERMIT_ALL_MESSAGE)
//    }
//
//    /** 这是spring boot 的一个bug **/
////    @Test
////    void testAccessClientMustHasRoleAdminSuccess(){
////        def accessToken = getToken(USER_USERNAME, USER_PASSWORD).access_token
////        def result = mockMvc.perform(
////                get(ACCESS_CLIENT_MUST_HAS_ADMIN_ROLE_URL).header('Authorization', "Bearer $accessToken")
////        ).andExpect(status().isOk())
////    }
//
//    @Test
//    void testAccessClientMustHasReadScopeSuccess(){
//        String accessToken = getToken(USER_USERNAME, USER_PASSWORD).access_token
//        def result = mockMvc.perform(
//                get(ACCESS_CLIENT_MUST_HAS_READ_SCOPE_URL).header('Authorization', "Bearer $accessToken")
//        ).andExpect(status().isOk())
//    }
//}
