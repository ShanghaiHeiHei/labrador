package com.labrador.mvcsample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class SampleController {

    @GetMapping("welcome")
    public String welcome(){
        return "welcome";
    }

    @GetMapping("needAuthenticated")
    public String needAuthenticated(){
        return "authenticated";
    }
}
